package com.example.gisrg.trinity_app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    SharedPreferences m_SP;
    private Vibrator m_vibrator;
    private Boolean m_use_vibrator = false;

    private TextView m_showStatus;
    private String m_status;
    private String m_stop;
    private String m_start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_showStatus = (TextView) findViewById(R.id.status);
        m_stop = "Trinity is not running";
        m_start = "Trinity is running";
        m_status = m_stop;

        m_vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        m_SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    }

    @Override
    protected void onStart() {
        super.onStart();
        m_use_vibrator = m_SP.getBoolean("vibrate", false);
    }

    public void Settings(View view) {
        if (m_use_vibrator) {
            m_vibrator.vibrate(200);
        }

        startActivity(new Intent(MainActivity.this, SettingsActivity.class));
    }

    public void Stop(View view) {
        if (m_status.equals(m_start)) {
            if (m_use_vibrator) {
                m_vibrator.vibrate(200);
            }
            m_status = m_stop;
            m_showStatus.setText(m_status);
        }
    }

    public void Start(View view) {
        if (m_status.equals(m_stop)) {
            if (m_use_vibrator) {
                m_vibrator.vibrate(200);
            }
            m_status = m_start;
            m_showStatus.setText(m_status);
        }
    }
}
